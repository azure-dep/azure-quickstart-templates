#!/bin/bash

OUTPUTFILE="/tmp/`basename $0`.log"
if [ -f ${OUTPUTFILE} ]; then 
    rm -f ${OUTPUTFILE}
fi

SERVER_NAME=$( uname -n )
SERVER_IP=$(ip route get 8.8.8.8 | awk '/8.8.8.8/ {print $3}')

# Public key's array to set into 'authorized_key' file
PUBKEYS_TO_COPY[0]="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAsMicXiH6F7zjhRrjLLyycFBUDHuMi4/wLuZtMteVt+lvC6/s8VtxMB+DDSUrctYV3Sp3Kn9r4tVGhaj6xsTcj45OpLJvSHd1rd+1ke8ehH7TrKWuvrIVkWCz78/1q+Ogx9Wc/c5eq9s5zRvSKFinLx1lxSPjytInlOUVYhp2H7Ofbz0YGItgCm67Cy38C8slmeHEP9EXFMEj4QKqv67leRdTy+POrYMWRqOtELGSIH7P11ImEcyvzldpInP6DOkEBzh7Zyr7059DOi4SuAVz9dSX6YFAZb0gllBU0qxey6z8HCsuZ68s7ahbsKSV9G4xEMAQFIuZtnfVjrK+TYzovw== valentino@albus.int.ingv.it"
PUBKEYS_TO_COPY[1]="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTPs3bE3LM0/YQADF6FO1/llv1/LjWI/P+qdtBOdpN6q0QJvxlYpx34uNgN1WD0x1Bb/rwU/aEXXMauhD+UuFEcs/IBi0/SrRYp8PhimFaZaFyUu/+biZ19zTlcSI3Rii9FOZfoWdet6JsDwquOz3BI+x28K8H2fHUWofBOS/eCfwxjQ80ncl/I0uUuXngnIyW6VACg88Peov6vau6BoJwumaBdB6G8PWizcm0vnTIXe+bf1FpMvAqT6jsLaqB6P0w5AAydLKoGSTjQDX1aiOKnMzMUXnFErwJfwzi553B6p4Do8EI82PZ7P2SSyN1BEH6wZZCF3KDADRr1ziER+EB valentino@pleo.int.ingv.it"
PUBKEYS_TO_COPY[2]="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAyJKkfDIcVk2lsfJJcWVNyp1ysMPSiRBAfYC7Oxbxb1tyfxCbWqPVVADxOpDc9XKQBpBKe8bc9UMHPu+hUGj+HgEvXKhowgcXXryavxx1njygdC6JCpxWmePVHloRoRGXTrOaDDQn560M4iDJ/BvqX9cH56c0lPcGT4m2zDrZ7lFxfJkOphCt2M16deX8/oM2RLYPN6WkawkjrTUl+78tp0OwPpiviTy9UuauaofQvuFVVbI6fQ5pqfFd3JbvBEsG/ViklFSfTyMMKBkuzmsKyffl1859yFwvA5AibBsccmpAhxIxP1PTw1nIJyurAeN0FsevyGpuwx/uH5yo5GsVTQ== mtheo@june.int.ingv.it"
PUBKEYS_TO_COPY[3]="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA1D5JiVLutCnzu7yco5M2NdfBgHcvp1TqMdotIu+b2tFKPnKf+Sp7OxKnnTPBq7EKcgprcAPcAwmHXOrhoebn+ZFeCI//EMmmodN2LsjGv6FUjgZwWAF6H5IY9miL7mhJSfBNkN24vCRdY5xSbX3gguQFre1+6A30JPaEAGUx41bpeGajPYZ7G3BPp7heoabJO9I8lZlbmFz5VZtv5eCjMvHj+Xh72nxKv37nzeI32rrCJiFGOD2ECnAITmGKxxrNvqVWih/j4OMLf43k+Q0cUEFi6qBgNGV6hZZFgSmBtT+1xdXiOX+sCM9mG2wBnzzi/qijQkxqcXJxs1KuldC5ow== stefano@excuse.int.ingv.it"
PUBKEYS_TO_COPY[4]="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCyAiQ+tycNr/gYUead9Jq5rFP8Jl+wpzkHTmKAOe2mUO3+MpZ0V9WiFCqmYAo6Kk0FRPh6KX1r2n+ICrhcdHOyQu1vdrmmJp77w03dVpLBDCu83hNIMHMS3jADaKm2QH6KfwBoxVDTi38bJRl9bGwBCiFMB0G0lgU+NlWLe8d7etuQcy9cLI5O7srHvucQu6ksMZv9m0YBNnlVPIkQcDW+9fvN9uEB6rJxsMYWZ8xy13ck/6V+ljYFuHBb7TVB26b4ADiae8DPnitlre1OveSdtU5vwLBFZJaamMV0LX9lY3/Z+MvkMGUza8KbfJpTYjbBkyX8k0XsyD8MqnfcHtTF root@osiride.int.ingv.it"
PUBKEYS_TO_COPY[5]="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCz0gMdip6O8B91/mOkx+e7mo3XDuPmcX3e3iFd4/3sf/cDN3GUmaon/NVLXmgCs9UCXK63lzFfFWwI59A95I1vEP7fuWUVdMg1rz1eKtsMZ1G1LOwuiYCtjWr+YltKt6KeNjwpeLO9xsXfEFkzDKBxhzDEAHqvpkdONyoTi97c0jMijpwxrpDruecr88L1rh4PME+haWWP/2RL3l6bnUvDzIz+krlN37XUgqUbElpY06yJk2xN8vwAD3h88m2+BAHM5YCCoYMbxhLauQUsq9HG+JNE/CSDI7AsNeHVt0tFQ0+hdxETszxnE7sPSQpliR1MBZi7yFyoInqdZR4nQlfP root@osiride2"

echo_date()
{
    echo "[$(date '+%Y-%M-%d %H:%m:%S')] - ${1}"
}

configure_public_keys()
{
    echo "Configure_public_keys:"
	OLD_IFS="${IFS}"
	IFS=$'\n'
	for PUBKEY_TO_COPY in ${PUBKEYS_TO_COPY[*]}
	do
            echo ${PUBKEY_TO_COPY}

            # root user
            echo ${PUBKEY_TO_COPY} >> ~/.ssh/authorized_keys

            # vmadmin user
            echo ${PUBKEY_TO_COPY} >> /home/${USER}/.ssh/authorized_keys
	done
	IFS="${OLD_IFS}"
    echo "Done"
    echo ""
}

configure_ulimit()
{
        echo_date "Set ulimit -n 65535"
        ulimit -n 65535
        echo_date "Done"
        echo ""
}

install_common_packages()
{
        PACKAGES="vim wget"
    	echo_date "Installing packages: \"${PACKAGES}\""

	# install needed bits in a loop because a lot of installs happen
	# on VM init, so won't be able to grab the dpkg lock immediately
	until sudo apt-get -y update && sudo apt-get -y install ${PACKAGES}
	do
		echo "Trying again"
		sleep 2
	done
	echo_date "Done"
	echo ""
}

install_sendmail()
{
        echo_date "Installing sendmail packages:"

        # install needed bits in a loop because a lot of installs happen
        # on VM init, so won't be able to grab the dpkg lock immediately
        until sudo apt-get -y update && sudo apt-get -y install sendmail
        do
                echo "Trying again"
                sleep 2
        done
        echo_date "Done"
        echo ""
}

install_docker()
{
echo_date "Install_docker"
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
echo "Done"
echo ""
}

run_docker()
{
    echo_date "Run docker container:"
    CONTAINER_NAME="nginx"
    if docker ps --format '{{.Names}}' | grep -q "^${CONTAINER_NAME}$"; then
        echo "${CONTAINER_NAME} container is already running; stop and remove."
        docker stop ${CONTAINER_NAME}
        docker container rm ${CONTAINER_NAME}
    else
        echo "${CONTAINER_NAME} is not running."
    fi

    echo "Start ${CONTAINER_NAME} container:"

    #sudo docker run -d --name nginx --restart always -p 80:80 vlauciani/osiride-reverse

    sudo docker login -u gitlab+deploy-token-220 -p glpat-o8XkAS5zap4YKckxE1DH https://gitlab.rm.ingv.it:5050
    sudo docker run -d --name nginx --restart always -p 80:80 gitlab.rm.ingv.it:5050/osiride/azure/docker-reverse-proxy:main

    echo_date "Done"
    echo ""
}

send_email()
{
# coma separated
#RECIPIENTS="valentino.lauciani@ingv.it,matteo.quintiliani@ingv.it"
RECIPIENTS="valentino.lauciani@ingv.it"

/usr/sbin/sendmail -v "${RECIPIENTS}" <<EOF
Subject:Azure - Started new instance "${SERVER_NAME}"
From:azure@azure.com

 DATE:        $( date +"%Y-%m-%d %H:%M:%S" )
 SERVER_NAME: ${SERVER_NAME}
 SERVER_IP:   ${SERVER_IP}

 OUTPUT:
 $( cat ${OUTPUTFILE} )

EOF
}

SEPLINE="============================================="

# Update "azureuser" password (can be used on Azure portal -> Scale Set -> Instances -> <vm> -> Serial console
echo "azureuser:t00r!toor!" | sudo chpasswd

echo "$SEPLINE" 2>&1 | tee -a ${OUTPUTFILE}
date  2>&1 | tee -a ${OUTPUTFILE}
echo "$@" 2>&1  | tee -a ${OUTPUTFILE}
echo "$SEPLINE" 2>&1  | tee -a ${OUTPUTFILE}
configure_public_keys 2>&1 | tee -a ${OUTPUTFILE}
configure_ulimit 2>&1 | tee -a ${OUTPUTFILE}
install_common_packages 2>&1 | tee -a ${OUTPUTFILE}
#install_sendmail 2>&1 | tee -a ${OUTPUTFILE}
#install_docker 2>&1 | tee -a ${OUTPUTFILE}
run_docker 2>&1 | tee -a ${OUTPUTFILE}
send_email  2>&1 | tee -a ${OUTPUTFILE}
